import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core'
import { Playlist } from './playlist';

@Component({
  selector: 'playlist-details',
  template: `
  <ng-container [ngSwitch]="mode">

    <div *ngSwitchDefault>
      <div class="form-group">
        <label for="">Name</label>
        <div class="form-control-static"> {{playlist.name}} </div>
      </div>
      <div class="form-group">
        <label for="">Favourite</label>
        <div class="form-control-static"> {{playlist.favourite? 'Yes' : 'No'}} </div>
      </div>
      <div class="form-group">
        <label for="">Color</label>
        <div class="form-control-static" [style.color]="playlist.color"> {{playlist.color}} </div>
      </div>
      <button (click)="mode = 'edit'">Edit</button>
    </div>

  <div *ngSwitchCase="'edit'">
    <div class="form-group">
      <label for="">Name</label>
      <input type="text" class="form-control"
            [(ngModel)]="playlist.name">
    </div>
    <div class="form-group">
      <label for="">Favourite</label>
      <input type="checkbox" 
            [(ngModel)]="playlist.favourite">
    </div>
    <div class="form-group">
      <label for="">Color</label>
      <input type="color" 
            [(ngModel)]="playlist.color">
    </div>
    <button>Save</button>
    <button (click)="mode = 'show'">Cancel</button>
  </div>
  `,
  styles: [],
  // inputs:[
  //   'playlist:playlist'
  // ],
  encapsulation: ViewEncapsulation.Emulated
})
export class PlaylistDetailsComponent implements OnInit {

  @Input()
  playlist:Playlist

  mode = 'show'

  constructor() { }

  ngOnInit() {
  }

}
