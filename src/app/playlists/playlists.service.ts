import { Injectable } from '@angular/core';
import { Playlist } from './playlist';

@Injectable()
export class PlaylistsService {

  playlists:Playlist[] = [
    { id:1, name:'Angular Hits', color:'#FF0000', favourite: false },
    { id:2, name:'Angular TOP20', color:'#00ff00', favourite: true },
    { id:3, name:'The best of angular', color:'#0000ff', favourite: false },
  ]

  getPlaylists(){
    return this.playlists
  }

  getPlaylist(id){
    return this.playlists.find( playlist => playlist.id == id)
  }


  constructor() { }

}
