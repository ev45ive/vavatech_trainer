import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'playlists-item',
  template: `
    {{playlist.name}}
  `,
  styles: [],
  encapsulation: ViewEncapsulation.Emulated
})
export class PlaylistsItemComponent implements OnInit {


  playlist

  constructor() { }

  ngOnInit() {
  }

}
