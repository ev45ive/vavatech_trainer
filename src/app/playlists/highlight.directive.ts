import { Directive, ElementRef, Input, HostListener, Renderer, HostBinding } from '@angular/core';

@Directive({
  selector: '[highlight]',
})
export class HighlightDirective {

  @Input('highlight')
  highlightColor = 'black'
  
  constructor(private elem:ElementRef,
  private renderer:Renderer) {
    // console.log(this.highlightColor)
  }

  
  @HostBinding('style.borderLeftColor')
  get getColor(){
    return this.hover? this.highlightColor : 'black'
  }

  hover = false
  
  @HostListener('mouseenter')
  enter(){
    // color = 'black'
    this.hover = true
  }

  @HostListener('mouseleave')
  exit(){
    this.hover = false
  }

  ngOnInit(){
    this.elem.nativeElement.style.borderLeftColor = this.highlightColor
  }

}
window['HighlightDirective'] = HighlightDirective
