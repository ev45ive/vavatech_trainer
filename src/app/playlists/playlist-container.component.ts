import { ActivatedRoute } from '@angular/router'
import { Component, OnInit } from '@angular/core';
import { PlaylistsService } from './playlists.service';

@Component({
  selector: 'playlist-container',
  template: ` 
    <playlist-details *ngIf="playlist"  [playlist]="playlist"></playlist-details>
    <div  *ngIf="!playlist">Please select playlist</div>
  `,
  styles: []
})
export class PlaylistContainerComponent implements OnInit {

  playlist

  constructor(private playlistService:PlaylistsService, private route:ActivatedRoute) { }

  ngOnInit() {
    // let id = parseInt(this.route.snapshot.params['id'],10)
    this.route.params.subscribe( params => {
      let id = parseInt(params['id'],10)
      this.playlist = this.playlistService.getPlaylist(id)
    })

  }

}
