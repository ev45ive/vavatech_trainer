import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core'
import { Playlist } from './playlist';

// [style.borderLeftColor]="(hover == playlist? playlist.color : 'black' )"
// (mouseenter)="hover = playlist"
// (mouseleave)="hover = false"

@Component({
  selector: 'playlists-list',
  template: `
      <div class="list-group">
        <div class="list-group-item" *ngFor="let playlist of playlists"
          [class.active]="selected == playlist"
         
          [highlight]="playlist.color"

          (click)="select(playlist)">
          {{playlist.name}}
        </div>
      </div>
  `,
  styles: [`
    .list-group-item{
      border-left: 10px solid transparent;
    }
  `],
  encapsulation: ViewEncapsulation.Emulated
})
export class PlaylistsListComponent implements OnInit {

  @Input()
  selected:Playlist

  @Output()
  selectedChange = new EventEmitter()

  select(playlist){
    this.selectedChange.emit(playlist)
  }

  @Input()
  playlists: Playlist[]

  constructor() { }

  ngOnInit() {
  }

}
