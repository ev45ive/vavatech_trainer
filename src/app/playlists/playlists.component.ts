import { Router, ActivatedRoute } from '@angular/router'
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Playlist } from './playlist';
import { PlaylistsService } from './playlists.service';

@Component({
  selector: 'playlists',
  template: `
    <p>
      playlists works!
    </p>
    <div class="row">
      <div class="col">
        <playlists-list 
        [playlists]="playlists"
        (selectedChange)="select($event)"
        [selected]="selected"></playlists-list>
      </div>
      <div class="col">
        <router-outlet></router-outlet>
      </div>
    </div>   
  `,
  styles: [],
  encapsulation: ViewEncapsulation.Emulated
})
export class PlaylistsComponent implements OnInit {

  playlists:Playlist[]

  constructor(private playlistService:PlaylistsService, 
    private route:ActivatedRoute,
    private router:Router) { }

  select(playlist){
    // this.selected = playlist
    this.router.navigate(['/playlists', playlist.id])
  }

  selected

  ngOnInit() {
    this.route.firstChild.params.subscribe(params => {
      let id = parseInt(params['id'])
      this.selected = this.playlistService.getPlaylist(id)
    })

    this.playlists = this.playlistService.getPlaylists()
  }

}
