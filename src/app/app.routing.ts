import { PlaylistContainerComponent } from './playlists/playlist-container.component'
import { MusicSearchComponent } from './music-search/music-search.component'
import { PlaylistsComponent } from './playlists/playlists.component'
import { Routes, RouterModule} from '@angular/router'

const routes:Routes = [
  { path:'', redirectTo:'playlists' , pathMatch:'full'},
  { path:'music', loadChildren: './music-search/music-search.module#MusicSearchModule' },
  { path:'playlists', component: PlaylistsComponent, children:[
    {path:'', component: PlaylistContainerComponent },
    {path:':id', component: PlaylistContainerComponent }

  ]},
  { path:'**', redirectTo:'music', pathMatch:'full' }
]

export const Routing = RouterModule.forRoot(routes,{
  // enableTracing:true,
  // useHash: true
})