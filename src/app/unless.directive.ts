import { Directive, ViewContainerRef, TemplateRef, Input } from '@angular/core';

@Directive({
  selector: '[unless]'
})
export class UnlessDirective {

  cache

  @Input()
  set unless(hide){
    if(hide){
      // this.vcr.clear()
      this.cache = this.vcr.detach()
    }else{
      if(this.cache){
        this.vcr.insert(this.cache)
      }else{
        this.vcr.createEmbeddedView(this.tpl,this.ctx)
      }
    }
  }

  constructor(private tpl:TemplateRef<any>,
    private vcr:ViewContainerRef) {
  
  }

  ctx = {
    $implicit:'placki',test:123
  }

  // UnlessOfType(type = 'promo'){

  // }

}
