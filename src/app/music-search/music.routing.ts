import { MusicSearchComponent } from './music-search.component'
import { Routes, RouterModule} from '@angular/router'

const routes:Routes = [
  { path:'', component: MusicSearchComponent },
]

export const Routing = RouterModule.forChild(routes)