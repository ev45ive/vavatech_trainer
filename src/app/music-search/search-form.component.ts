import { debounceTime, filter } from 'rxjs/operators'
import { FormControlName } from '@angular/forms/src/directives'
import {
  AbstractControl,
  AsyncValidatorFn,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms'
import { Component, EventEmitter, OnInit, Output } from '@angular/core'
import { Observable } from 'rxjs/Observable';

//https://getbootstrap.com/docs/4.0/components/input-group/#button-addons
@Component({
  selector: 'search-form',
  template: `
    <form [formGroup]="queryForm">
      <div class="input-group mb-2">
        <input type="text" class="form-control"
        formControlName="query"
        (keyup.enter)="search(ref.value)"
        placeholder="Search for..." #ref>
        <span class="input-group-btn">
          <button class="btn btn-secondary" type="button" (click)="search(ref.value)">Go!</button>
        </span>
      </div>
      <div *ngIf="queryForm.pending">Please wait...</div>
      <div *ngIf="queryForm.get('query').touched || queryForm.get('query').dirty">
        <div *ngIf="queryForm.get('query').hasError('required')">Field is required</div>
        <div *ngIf="queryForm.get('query').hasError('minlength')">value too short </div>
        <div *ngIf="queryForm.get('query').hasError('badword')">Can't search for that! </div>
      </div>
    </form>
  `,
  styles: [`
    form .ng-dirty.ng-invalid,
    form .ng-touched.ng-invalid {
        border: 2px solid red !important;
    }
  `]
})
export class SearchFormComponent implements OnInit {

  queryForm:FormGroup

  constructor(private builder:FormBuilder) {
    // this.queryForm = new FormGroup({
    //   'query': new FormControl('batman')
    // })

    const censor = (word:string):ValidatorFn => (control:AbstractControl):ValidationErrors|null => {
      if(control.value.indexOf(word) != -1){
        return {
          'badword':word
        }
      }else{
        return null
      }
    }
    type OptionalErrors = ValidationErrors|null;

    const asyncCensor = (word):AsyncValidatorFn => (control:AbstractControl):Observable<OptionalErrors> => {

      return Observable.create( observer => {
        setTimeout(()=>{
          let isError = (control.value.indexOf(word) != -1)? {'badword':word} : null; 
          
          observer.next(isError)
          observer.complete()
        },2000)
      })
    }


    this.queryForm = this.builder.group({
      query: this.builder.control('',[
        Validators.required,
        Validators.minLength(3),
        // censor('superman')
      ],[
        asyncCensor('superman')
      ])
    })
    window['form'] = this.queryForm

    this.queryForm.get('query').valueChanges
    .pipe(
      filter(query => query.length >= 3),
      debounceTime(400)
    )
    .subscribe(query => {
      this.search(query)
    })
  }

  search(query){
    // console.log(query)
    this.queryChange.emit(query)
  }

  @Output()
  queryChange = new EventEmitter()

  ngOnInit() {
  }

}
