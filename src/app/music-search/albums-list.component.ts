import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'albums-list',
  template: `
    <div class="card-group">
      <album-item class="card" [album]="album" *ngFor="let album of albums">
      </album-item>
    </div>
  `,
  styles: [`
    .card{
      min-width:25%;
    }
  `]
})
export class AlbumsListComponent implements OnInit {

  @Input()
  albums

  constructor() { }

  ngOnInit() {
  }

}
