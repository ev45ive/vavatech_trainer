import { Routing } from './music.routing'
import { ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicSearchComponent } from './music-search.component';
import { SearchFormComponent } from './search-form.component';
import { AlbumsListComponent } from './albums-list.component';
import { AlbumItemComponent } from './album-item.component';
import { MusicService } from './music.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    Routing
  ],
  declarations: [MusicSearchComponent, SearchFormComponent, AlbumsListComponent, AlbumItemComponent],
  providers: [
    // { provide: 'music_api_url', useValue: 'URL' },
    // {
    //   provide: MusicService, useFactory: (url) => {
    //     return new MusicService(url)
    //   }, deps: ['music_api_url']
    // },
    // { provide: AbstractMusicService, useClass: MusicService }
    MusicService
  ],
  exports: [
    // MusicSearchComponent
  ]
})
export class MusicSearchModule { }
