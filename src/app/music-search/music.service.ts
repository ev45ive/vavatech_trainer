
import { map } from 'rxjs/operators'
import { AuthService } from '../auth/auth.service'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/catch'

@Injectable()
export class MusicService {

  constructor(private http:HttpClient, private auth:AuthService) { }

  getAlbums(query = 'batman'){
    return this.http.get<any>(`https://api.spotify.com/v1/search?type=album&q=${query}`,{
      headers: new HttpHeaders({
        'Authorization': 'Bearer '+this.auth.getToken()
      })
    }).pipe(
      map(data => data.albums.items),
    )
    .catch(err =>{
      this.auth.authorize()
      return []
    })
  }

}
