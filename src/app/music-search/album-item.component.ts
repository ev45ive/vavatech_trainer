import { AlbumImage, Album } from './interfaces'
import { Component, Input, OnInit } from '@angular/core'

@Component({
  selector: 'album-item',
  template: `
  <img class="card-img-top" [src]="image.url">
  <div class="card-body">
    <h4 class="card-title">
      {{album.name}}
    </h4>
  </div>
  `,
  styles: []
})
export class AlbumItemComponent implements OnInit {

  @Input('album')
  set setAlbum(album){
    this.album = album
    this.image = album.images[0]
  }
  image:AlbumImage

  album:Album

  constructor() { }

  ngOnInit() {
  }

}
