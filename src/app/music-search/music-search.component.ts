import { Subscription } from 'rxjs/Subscription'
import { MusicService } from './music.service'
import { Component, OnInit } from '@angular/core';
import { Album } from './interfaces';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'music-search',
  template: `
    <div class="row">
      <div class="col">
        <search-form 
          (queryChange)="search($event)"></search-form>
      </div>
    </div>
    <div class="row">
      <div class="col">
      
        <albums-list [albums]="albums$ | async "></albums-list>
      </div>
    </div>
  `,
  styles: []
})
export class MusicSearchComponent implements OnInit {

  constructor(private musicService:MusicService) { }

  albums:Album[]

  // subscripion:Subscription

  albums$:Observable<Album[]>

  search(query){
    this.albums$ = this.musicService.getAlbums(query)
    // .subscribe(albums => {
    //   this.albums = albums
    // })
  }

  // ngOnDestroy(){
  //   this.subscripion.unsubscribe()
  // }

  ngOnInit() {
    this.search('batman')
  }

}
