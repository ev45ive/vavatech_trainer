interface Entity{
  id: string;
  name: string;
}

export interface Album extends Entity {
  artits: Artist[];
  images: AlbumImage[];
}

export interface Artist extends Entity{

}

export interface AlbumImage{
  url: string;
  height: number;
  width: number;
}