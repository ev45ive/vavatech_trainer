import { FormsModule } from '@angular/forms'
import { By } from '@angular/platform-browser'
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestingComponent } from './testing.component';

fdescribe('TestingComponent', () => {
  let component: TestingComponent;
  let fixture: ComponentFixture<TestingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[FormsModule],
      declarations: [ TestingComponent ],
      providers:[]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render text', () => {
    expect(fixture.debugElement.query(By.css('p')).nativeElement.textContent).toEqual('testing');
  });

  it('should update text', () => {
    component.message = 'test'
    fixture.detectChanges()
    expect(fixture.debugElement.query(By.css('p')).nativeElement.textContent).toEqual('test');
  });

  it('should show ngModel', () => {
    component.message = 'test'
    fixture.detectChanges()
  

    fixture.whenRenderingDone().then(()=>{

      expect(fixture.debugElement.query(By.css('input')).nativeElement.value).toEqual('test');
    })
  });

});
