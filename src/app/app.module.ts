import { PlaylistsService } from './playlists/playlists.service'
import { AuthService } from './auth/auth.service'
import { MusicSearchModule } from './music-search/music-search.module'
import { FormsModule } from '@angular/forms'
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PlaylistsComponent } from './playlists/playlists.component';
import { PlaylistsListComponent } from './playlists/playlists-list.component';
import { PlaylistsItemComponent } from './playlists/playlists-item.component';
import { PlaylistDetailsComponent } from './playlists/playlist-details.component';
import { HighlightDirective } from './playlists/highlight.directive';
import { UnlessDirective } from './unless.directive';
import { Routing } from './app.routing';
import { PlaylistContainerComponent } from './playlists/playlist-container.component';
import { TestingComponent } from './testing.component';


@NgModule({
  declarations: [
    AppComponent,
    PlaylistsComponent,
    PlaylistsListComponent,
    PlaylistsItemComponent,
    PlaylistDetailsComponent,
    HighlightDirective,
    UnlessDirective,
    PlaylistContainerComponent,
    TestingComponent
  ],
  imports: [
    BrowserModule,
    MusicSearchModule,
    FormsModule,
    Routing
  ],
  providers: [AuthService, PlaylistsService],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(private auth:AuthService){
    this.auth.getToken()
  }
}
