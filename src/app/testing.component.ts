import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'testing',
  template: `
    <p>{{message}}</p>
    <input [(ngModel)]="message">
  `,
  styles: []
})
export class TestingComponent implements OnInit {
  
  message = 'testing'

  constructor() { }

  ngOnInit() {
  }

}
